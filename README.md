README
=====

```
$ git remote add docs git@bitbucket.org:iFromz/docs.git
$ git subtree add --prefix=docs/ docs master
$ git subtree pull --prefix=docs docs master

```


### Setting up the empty repository for a subtree merge

```
$ mkdir test
$ cd test

$ git init
$ touch .gitignore
$ git add .gitignore
$ git commit -m "initial commit"
```

### Adding a new repository as a subtree
```
$ git remote add -f spoon-knife git@github.com:octocat/Spoon-Knife.git
$ git merge -s ours --no-commit spoon-knife/master
$ git read-tree --prefix=spoon-knife/ -u spoon-knife/master
$ git commit -m "Subtree merged in spoon-knife"
```

### Synchronizing with updates and changes
```
$ git pull -s subtree spoon-knife master
```
